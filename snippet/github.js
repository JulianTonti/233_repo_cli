/*

# Day 170: 2018-Jun-20 (Wed): 

Functions for interacting with GitHub snippets (gists).

*/
const sh = require('shelljs'); sh.config.silent = true;
const fs = require('fs');
const path = require('path');
const request = require('request');

// given the username and password, create an authentication token
function authenticate(callback, {username='',password=''})
{
	const TOKEN_NAME = "repo-cli-github";

	// see: https://developer.github.com/v3/oauth_authorizations/
	request(
		{
			url    : 'https://api.github.com/authorizations',
			method : 'POST',
			auth   : {
				'user': username,
				'pass': password
			},
			headers : {
				'User-Agent' : 'request'
			},
			body : JSON.stringify({
				scopes: ["repo", "delete_repo", "gist"],
				note: TOKEN_NAME
			})
		},
		function (error, response_object, response_body)
		{
			try { response_body = JSON.parse(response_body); } catch (e) {};
			if (error || (response_object && response_object.statusCode != 201))
			{
				console.error('FAILURE: unable to create access token.');
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.error(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (error) console.error(error);
				if (response_body) console.error(response_body);
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(error || response_object, response_body);
			}
			else
			{
				console.log('SUCCESS: access token created.');
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.log(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (response_body) console.log(response_body);
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(null, response_body.token);
			}
		}
	);
}

// given an authentication token and username, get a list of all the user's snippets
function list(callback, {username='', token=''})
{
	// see: https://developer.github.com/v3/gists/#list-a-users-gists
	request(
		{
			url : 'https://api.github.com/gists',
			method : 'GET',
			headers : {
				'User-Agent'    : 'request',
				'Authorization' : `token ${token}`
			}
		},
		function (error, response_object, response_body)
		{
			try { response_body = JSON.parse(response_body); } catch (e) {};
			if (error || (response_object && response_object.statusCode != 200))
			{
				console.error('FAILURE: unable to get snippet list.');
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.error(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (error) console.error(error);
				if (response_body) console.error(response_body);
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(error || response_object, response_body);
			}
			else
			{
				console.log('SUCCESS: got snippet list.');
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.log(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (response_body) console.log(response_body);
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(null, response_body);
			}
		}
	);
}

// open the HTML page for the specified snippet (by its id)
function open(callback, {username='', reponame=''})
{
	sh.exec(`open https://gist.github.com/${reponame}`);
	//sh.exec(`open https://github.com/${username}/${reponame}`);
}

// create a new remote snippet from a local directory that may be already under revision control
function create(callback=()=>{}, {sourcedir='', username='', token='', repoinfo='', repoaccess='private'})
{
	// extract file content
	let files = {};

	for (let ifile of sh.ls(sourcedir))
	{
		let full = path.resolve(sourcedir + '/' + ifile);
		let base = path.basename(full);
		
		// abort if an entity in the input directory is not a file
		if (sh.test('-f',full) === false) {
			return callback(`ERROR: input directory can only contain files`, full);
		}
		files[base] = { content : fs.readFileSync(full).toString() };
	}

	// see: https://developer.github.com/v3/gists/#create-a-gist
	request(
		{
			url    : 'https://api.github.com/gists',
			method : 'POST',
			headers : {
				'User-Agent'    : 'request',
				'Authorization' : `token ${token}`
			},
			body : JSON.stringify({
				files       : files,
				description : repoinfo,
				public      : repoaccess == 'public'
			})
		},
		function (error, response_object, response_body)
		{
			try { response_body = JSON.parse(response_body); } catch (e) {};
			if (error || (response_object && response_object.statusCode != 201))
			{
				console.error('FAILURE: unable to create remote repo.');
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.error(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (error) console.error(error);
				if (response_body) console.error(response_body);
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(error || response_object, response_body);
			}
			else
			{
				console.log('SUCCESS: remote repo created.');
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.log(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				//if (response_body) console.log(response_body);
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');

				//git commands
				let url = response_body.git_push_url;
				sh.cd(sourcedir);
				sh.exec('git init');
				sh.exec(`git remote add origin ${response_body.git_push_url}`);
				sh.exec(`git remote set-url origin ${response_body.git_push_url}`);
				sh.exec('git fetch --all');
				sh.exec('git reset --hard origin/master')
				sh.exec('git add *');
				sh.exec('git commit -m "first commit"');
				sh.exec('git push -u origin master');
				sh.exec(`echo "${response_body.owner.login}"`);
				sh.exec(`echo "${token}"`);

				callback(null, response_body);
			}
		}
	);
}

// delete a remote snippet
function destroy(callback, {username='', reponame='', token=''})
{
	// see: https://developer.github.com/v3/gists/#delete-a-gist
	request(
		{
			url    : `https://api.github.com/gists/${reponame}`,
			method : 'DELETE',
			headers : {
				'User-Agent'    : 'request',
				'Authorization' : `token ${token}`
			}
		},
		function (error, response_object, response_body)
		{
			try { response_body = JSON.parse(response_body); } catch (e) {};
			if (error || (response_object && response_object.statusCode != 204))
			{
				console.error('FAILURE: unable to delete remote repo.');
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.error(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (error) console.error(error);
				if (response_body) console.error(response_body);
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(error || response_object, response_body);
			}
			else
			{
				console.log('SUCCESS: remote repo deleted.');
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.log(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (response_body) console.log(response_body);
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(null, response_body);
			}
		}
	);
}

module.exports = { authenticate, list, open, create, destroy };
