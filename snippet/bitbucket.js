/*

# Day 170: 2018-Jun-20 (Wed): 

Functions for interacting with BitBucket snippets.

*/
const sh = require('shelljs'); sh.config.silent = true;
const fs = require('fs');
const path = require('path');
const request = require('request');

// given the username and password, create an authentication token
function authenticate(callback, {username='',password=''})
{
	callback(`\n
	ERROR: at the time of writing, it is not possible to create an application 
	password via the BitBucket REST API. You will have to create one manually
	using the following page: 
	
	https://bitbucket.org/account/user/${username}/app-passwords/new

	- set snippet access to Read & Write.
	- create and record the token in ~/.repo-cli.json under bitbucket.token`);
}

// given an authentication token and username, get a list of all the user's snippets
function list(callback, {username='', token=''})
{
	// see: https://developer.atlassian.com/bitbucket/api/2/reference/resource/snippets/%7Busername%7D
	request(
		{
			url    : `https://api.bitbucket.org/2.0/snippets/${username}?pagelen=100`,
			method : 'GET',
			headers : {
				'User-Agent'    : 'request',
				//'Authorization' : `Bearer ${params.token}`
			},
			auth : {
				'username' : username,
				'password' : token
			}
		},
		function (error, response_object, response_body)
		{
			try { response_body = JSON.parse(response_body); } catch (e) {};
			if (error || (response_object && response_object.statusCode != 200))
			{
				console.error('FAILURE: unable to list repos.');
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.error(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (error) console.error(error);
				if (response_body) console.error(response_body);
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(error || response_object, response_body);
			}
			else
			{
				console.log('SUCCESS: got repo list.');
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.log(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (response_body) console.log(response_body);
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				response_body = response_body.values.map(item => ({
					id     : item.id,
					info   : item.title,
					access : item.is_private ? 'private' : 'public',
					url    : item.links.self,
				}));
				console.log(response_body);
				callback(null, response_body);
			}
		}
	);
}

// open the HTML page for the specified snippet (by its id)
function open(callback, {username='', reponame=''})
{
	sh.exec(`open https://bitbucket.org/snippets/${username}/${reponame}`);
}

// create a new remote snippet from a local directory that may be already under revision control
function create(callback=()=>{}, {sourcedir='', username='', token='', repoinfo='', repoaccess='private'})
{
	console.error('NOT IMPLEMENTED YET');
	process.exit();

	// extract file content
	let files = {};

	for (let ifile of sh.ls(params.sourcedir))
	{
		let full = path.resolve(params.sourcedir + '/' + ifile);
		let base = path.basename(full);
		if (sh.test('-f',full) === false) continue;
		files[base] = { content : fs.readFileSync(full).toString() };
		break;
	}
	// see: https://developer.atlassian.com/bitbucket/api/2/reference/resource/snippets/%7Busername%7D#post
	request(
		{
			url    : `https://api.bitbucket.org/2.0/snippets/${params.username}`,
			method : 'POST',
			headers : {
				'User-Agent'    : 'request',
				'Content-Type'  : 'application/json',

				//'Authorization' : `token ${params.token}`
			},
			auth : {
				'username' : params.username,
				'password' : params.token
			},
			body : JSON.stringify({
				scm        : 'git',
				is_private : params.repoaccess == 'private',
				title      : params.repoinfo,
				files      : files
			})
		},
		function (error, response_object, response_body)
		{
			try { response_body = JSON.parse(response_body); } catch (e) {};
			if (error || (response_object && response_object.statusCode != 201))
			{
				console.error('FAILURE: unable to create snippet.');
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.error(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (error) console.error(error);
				if (response_body) console.error(response_body);
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(error || response_object, response_body);
			}
			else
			{
				console.log('SUCCESS: snippet created.');
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.log(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				//if (response_body) console.log(response_body);
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(null, response_body);

				// //git commands
				// let url = response_body.git_push_url;
				// sh.cd(params.sourcedir);
				// sh.exec('git init');
				// sh.exec(`git remote add origin ${response_body.git_push_url}`);
				// sh.exec(`git remote set-url origin ${response_body.git_push_url}`);
				// sh.exec('git fetch --all');
				// sh.exec('git reset --hard origin/master')
				// sh.exec('git add *');
				// sh.exec('git commit -m "first commit"');
				// sh.exec('git push -u origin master');
				// sh.exec(`echo "${response_body.owner.login}"`);
				// sh.exec(`echo "${params.token}"`);
			}
		}
	);
}

// delete a remote snippet
function destroy(callback, {username='', reponame='', token=''})
{
	// see: https://developer.atlassian.com/bitbucket/api/2/reference/resource/snippets/%7Busername%7D/%7Bencoded_id%7D#delete
	request(
		{
			url    : `https://api.bitbucket.org/2.0/snippets/${username}/${reponame}`,
			method : 'DELETE',
			headers : {
				'User-Agent'    : 'request'
			},
			auth : {
				'username' : username,
				'password' : token
			}
		},
		function (error, response_object, response_body)
		{
			try { response_body = JSON.parse(response_body); } catch (e) {};
			if (error || (response_object && response_object.statusCode != 204))
			{
				console.error('FAILURE: unable to delete snippet.');
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.error(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (error) console.error(error);
				if (response_body) console.error(response_body);
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(error || response_object, response_body);
			}
			else
			{
				console.log('SUCCESS: snippet deleted.');
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.log(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (response_body) console.log(response_body);
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(null, response_body);
			}
		}
	);
}

module.exports = { authenticate, list, open, create, destroy };
