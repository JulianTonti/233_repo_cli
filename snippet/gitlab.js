/*

# Day 170: 2018-Jun-20 (Wed): 

Functions for interacting with GitHub snippets (gists).

*/
const sh = require('shelljs'); sh.config.silent = true;
const fs = require('fs');
const path = require('path');
const request = require('request');

const URL = 'https://gitlab.com/api/v4/';
const TOKEN = '-_JM3yz4wkyPu5zzUCWh';

// given the username and password, create an authentication token
function authenticate(callback, {username='',password=''})
{
	callback(`\n
	ERROR: at the time of writing, it is not possible to create an application 
	password via the GitLab REST API. You will have to create one manually
	using the following page: 
	
	https://gitlab.com/profile/personal_access_tokens
	
	- tick the API scope
	- create and record the token in ~/.repo-cli.json under tokens.gitlab`);
}

// given an authentication token and username, get a list of all the user's snippets
function list(callback, {username='', token=''})
{
	// see: https://docs.gitlab.com/ee/api/snippets.html#list-snippets
	request(
		{
			url : 'https://gitlab.com/api/v4/snippets?per_page=100',
			method : 'GET',
			headers : {
				'Content-Type'  : 'application/json',
				'User-Agent'    : 'request',
				'Private-Token' : token
			}
		},
		function (error, response_object, response_body)
		{
			try { response_body = JSON.parse(response_body); } catch (e) {};
			if (error || (response_object && response_object.statusCode != 200))
			{
				console.error('FAILURE: unable to get snippet list.');
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.error(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (error) console.error(error);
				if (response_body) console.error(response_body);
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(error || response_object, response_body);
			}
			else
			{
				response_body = response_body.map(item => ({
					id     : item.id,
					name   : item.file_name,
					info   : item.description,
					access : item.visibility,
					url    : item.web_url,
				}));

				console.log('SUCCESS: got snippet list.');
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.log(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (response_body) console.log(response_body);
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(null, response_body);
			}
		}
	);
}

// open the HTML page for the specified snippet (by its id)
function open(callback, {username='', reponame=''})
{
	sh.exec(`open https://gitlab.com/api/v4/snippets/${reponame}`);
	//sh.exec(`open https://github.com/${username}/${reponame}`);
}

// create a new remote snippet from a local directory that may be already under revision control
function create(callback=()=>{}, {sourcedir='', username='', token='', reponame='', repoinfo='', repoaccess='private'})
{
	// see: https://docs.gitlab.com/ee/api/snippets.html#create-new-snippet
	request(
		{
			url    : 'https://gitlab.com/api/v4/snippets',
			method : 'POST',
			headers : {
				'User-Agent'    : 'request',
				'Content-Type'  : 'application/json',
				'Private-Token' : token
			},
			body : JSON.stringify({
				title       : reponame,
				file_name   : reponame,
				content     : fs.readFileSync(sourcefile).toString(),
				description : repoinfo,
				visibility  : repoaccess == 'public'
			})
		},
		function (error, response_object, response_body)
		{
			try { response_body = JSON.parse(response_body); } catch (e) {};
			if (error || (response_object && response_object.statusCode != 201))
			{
				console.error('FAILURE: unable to create remote repo.');
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.error(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (error) console.error(error);
				if (response_body) console.error(response_body);
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(error || response_object, response_body);
			}
			else
			{
				console.log('SUCCESS: remote repo created.');
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.log(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				//if (response_body) console.log(response_body);
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');

				//git commands
				let url = response_body.git_push_url;
				sh.cd(sourcedir);
				sh.exec('git init');
				sh.exec(`git remote add origin ${response_body.git_push_url}`);
				sh.exec(`git remote set-url origin ${response_body.git_push_url}`);
				sh.exec('git fetch --all');
				sh.exec('git reset --hard origin/master')
				sh.exec('git add *');
				sh.exec('git commit -m "first commit"');
				sh.exec('git push -u origin master');
				sh.exec(`echo "${response_body.owner.login}"`);
				sh.exec(`echo "${token}"`);

				callback(null, response_body);
			}
		}
	);
}

// delete a remote snippet
function destroy(callback, {username='', reponame='', token=''})
{
	console.warn('WARNING: GitLab snippets are not revision controlled');
	
	// see: https://docs.gitlab.com/ee/api/snippets.html#delete-snippet
	request(
		{
			url    : `https://gitlab.com/api/v4/snippets/${reponame}`,
			method : 'DELETE',
			headers : {
				'User-Agent'    : 'request',
				'Private-Token' : token
			}
		},
		function (error, response_object, response_body)
		{
			try { response_body = JSON.parse(response_body); } catch (e) {};
			if (error || (response_object && response_object.statusCode != 204))
			{
				console.error('FAILURE: unable to delete remote snippet.');
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.error(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (error) console.error(error);
				if (response_body) console.error(response_body);
				console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(error || response_object, response_body);
			}
			else
			{
				console.log('SUCCESS: remote snippet deleted.');
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				if (response_object) console.log(`HTTP ${response_object.statusCode}: ${response_object.statusMessage}`);
				if (response_body) console.log(response_body);
				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
				callback(null, response_body);
			}
		}
	);
}

module.exports = { authenticate, list, open, create, destroy };
