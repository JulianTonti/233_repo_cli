# repo-cli

A command line tool for BitBucket, GitLab & GitHub code repositories.

---

## Installation

```bash
# Download and install.
git clone https://bitbucket.org/JulianTonti/233_repo_cli
cd 233_repo_cli/
npm install

# Create alias (use ~/.bash_profile on Mac).
alias repo="node `pwd`/cli/cli.js"
echo "alias repo=\"node `pwd`/cli/cli.js\"" >> ~/.bashrc

# Assign personal access tokens.
repo github setup
repo gitlab setup
repo bitbucket setup

# Restrict access to your config file.
chmod 600 ~/.repo-cli.json

# Configure defaults (see below for options).
vi ~/.repo-cli.json
```

---

## Usage

```bash
repo --help
repo <remote> setup
repo <remote> list
repo <remote> url
repo <remote> open
repo <remote> exists
repo <remote> create
repo <remote> delete
repo <remote> select
repo <remote> config
repo <remote> update <key> <value>
repo <remote> public
repo <remote> private
repo <remote> clone [<local>]
repo <remote> push [<local>]
repo <remote> pull [<local>]
```

---

## Where

```
<remote>
The remote repository host and repo name in the format host://name. Supported
hosts are 'github', 'gitlab' and 'bitbucket'. Example: github://MyRepo

<local>
A relative or absolute filesystem path to a directory that will be put under  
Git version control and linked to the remote repo. If the directory is already  
under control then its remote origin will be redirected to the new host. If  
not specified, then the current directory will be used. NOTE: the directory  
basename and the remote repo name must match.

<key> <value>
A configurable metadata option for the repo that can be changed with the repo  
host. To see a list of configurable options, use repo \<remote> config.
```

---

## Notes

- a configuration file will be created in ~/.repo-cli.json
- add your authentication tokens to the config file.
- program defaults can also be set in the config file.
- the revision control engine is Git.
- if a project is already under revision control, its remote will be redirected

---

```
repo --help
  Show this help information.

repo setup
  Set up remote repo hosts, usernames and access tokens.

repo <remote> list
  Print a list of repos at the specified host (only the hostname is required
  not the reponame).

repo <remote> url
  Print the remote address (url) of the repo.

repo <remote> open
  Using the system's default browser, open the homepage for the specified repo.

repo <remote> exists
  Checks the remote host to see if a repo exists.

repo <remote> create
  Create a blank repo at the remote host (if possible).

repo <remote> delete
  Delete the specified remote repo (if an access token has been provided).

repo <remote> select
  Print all metadata for the specified repo.

repo <remote> config
  Print only the configurable subset of metadata for a repo.

repo <remote> public|private|internal
  Set the visibility of a repo. Internal is only supported by GitLab.

repo <remote> update <key> <value>
  Edit a specific element of configurable metadata for a repo.

repo <remote> clone [<local>]
  Clone the remote repo into the specified local directory (default: './')
  - local basename must match remote name
  - local directory will be created if it doesn't yet exist

repo <remote> push [<local>]
  Push changes from the local directory (default './') to the specified repo.
  - repo name and local basename must match.
  - local directory will be put under Git control if not already
  - all local changes will be automatically committed with a generic message

repo <remote> pull [<local>]
  Pull changes from the remote repo to the specified local directory (or './')
```

### Examples

Follow this sequence of commands to get a feel for the utility.
Substitute bitbucket with gitlab or github as desired.

```bash
# set bitbucket access token
repo bitbucket setup

# create a test repo
repo bitbucket://test create

# check the repo exists now
repo bitbucket://test exists

# get the URL for the repo
repo bitbucket://test url

# open the repo's page in your default browser
repo bitbucket://test open

# see a list of all repos. Look for the test repo
repo bitbucket list

# see all metadata for the repo (JSON)
repo bitbucket://test select

# see only the metadata that is editable
repo bitbucket://test config

# change some metadata elements (use JSON formatting for values)
repo bitbucket://test update description '"test repo"'
repo bitbucket://test update has_wiki true

# make the repo to public
repo bitbucket://test public

# delete the repo
repo bitbucket://test delete

# verify that the repo no longer exists
repo bitbucket://test exists
```

---

## List of configuration options

- Default options can be set by manually editing `~/.repo-cli.json` or by calling `repo config <key=val>`
- If one-time options are provided as `<key=val>` pairs with `repo create`, then these will override the defaults.
- Values should be quoted if they contain spaces.
- Setting a value to `null` or `undefined` with `repo config` will delete the default key/val pair.

### Core options

These options are shared across all three repo hosts.

- **host** : {text} [`github` | `gitlab` | `bitbucket`]
- **user** : {text} Your username for the specified host
- **token** : {text} Your access token for the specified host (don't use your password!).
- **access** : {text} [`public` | `private` | `internal`].
- **name** : {text} Case sensitive name of the repo.
- **info** : {text} Short description of the repo (preferably 1-2 lines).

### Additional GitHub-specific options

See: [GitHub REST API](https://developer.github.com/v3/repos/#create)

- name
- description
- homepage
- private
- has_issues
- has_projects
- has_wiki
- team_id
- auto_init
- gitignore_template
- license_template
- allow_squash_merge
- allow_merge_commit
- allow_rebase_merge

### Additional GitLab-specific options

See: [GitLab REST API](https://docs.gitlab.com/ee/api/projects.html#create-project)

- name
- path
- namespace_id
- default_branch
- description
- issues_enabled
- merge_requests_enabled
- jobs_enabled
- wiki_enabled
- snippets_enabled
- resolve_outdated_diff_discussions
- container_registry_enabled
- shared_runners_enabled
- visibility
- import_url
- public_jobs
- only_allow_merge_if_pipeline_succeeds
- only_allow_merge_if_all_discussions_are_resolved
- merge_method
- lfs_enabled
- request_access_enabled
- tag_list
- avatar
- printing_merge_request_link_enabled
- ci_config_path
- repository_storage
- approvals_before_merge

### Additional BitBucket-specific options

See: [BitBucket REST API](https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D#post)

- links
- uuid
- full_name
- is_private
- scm
- name
- description
- created_on
- updated_on
- size
- language
- has_issues
- has_wiki
- fork_policy
