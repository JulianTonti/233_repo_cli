#!/usr/bin/env node
/*

# Day 169: 2018-Jun-19 (Tue): 

Generic CLI for use with any repo host.

*/
// process.argv = process.argv.join('\0').split(/\0?=\0?/).join("=").split("\0");
const docs = require('fs').readFileSync(`${__dirname}/docs.txt`).toString();
const opts = require('docopt').docopt(docs);
const host = require('../lib/host.js');

// general error handler
function conclude(error)
{
	if (!error) return;
	let {message,state} = error;

	console.error(message);
	
	if (state) {
		console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
		if (state.meta) console.error(`HTTP ${state.meta.statusCode}: ${state.meta.statusMessage}`);
		if (state.error) console.error(state.error);
		if (state.data) console.error(state.data);
		console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
	}
	process.exit();
}

// run the specified command
try {
	if (opts.setup)        host.authenticate(opts["<remote>"]);
	else if (opts.list)    host.list(opts["<remote>"], (e,d) => { conclude(e); console.log(d.map(i => `[${i.access}] ${i.name}`).join("\n")); });
	else if (opts.url)     console.log(host.get_url(opts["<remote>"]));
	else if (opts.open)    host.open(opts["<remote>"]);
	else if (opts.exists)  host.exists(opts["<remote>"], (e,d) => { conclude(e); console.log(d ? 'true' : 'false'); })
	else if (opts.create)  host.create(opts["<remote>"], {}, conclude);
	else if (opts.delete)  host.destroy(opts["<remote>"], conclude);
	else if (opts.select)  host.select(opts["<remote>"], (e,d) => { conclude(e); console.log(JSON.stringify(d.data,null,2)); });
	else if (opts.config)  host.select(opts["<remote>"], (e,d) => { conclude(e); console.log(JSON.stringify(d.config,null,2)); });
	else if (opts.public)  host.set_access(opts["<remote>"], 'public', conclude);
	else if (opts.private) host.set_access(opts["<remote>"], 'private', conclude);
	else if (opts.update)  host.modify(opts["<remote>"], {[opts["<key>"]]:JSON.parse(opts["<value>"])}, conclude);
	else if (opts.clone)   host.clone(opts["<remote>"], opts["<local>"] || './');
	else if (opts.pull)    host.pull(opts["<remote>"], opts["<local>"] || './');
	else if (opts.push)    host.push(opts["<remote>"], opts["<local>"] || './', opts["<message>"]);
	else throw "ERROR: command not implemented";
}
catch (e) {
	conclude({
		message : `ERROR: command failed`,
		state : {error:e}
	});
}
