Information about BitBucket personal access tokens:
  https://confluence.atlassian.com/bitbucketserver/personal-access-tokens-939515499.html

Your configuration file should be located in your home directory:
  ${HOME}/.repo_cli.json

Add one or more tokens to your configuration file:

0) Browse to https://bitbucket.org/account/user/${your_username}/app-passwords/new
1) Enter a token name, eg: repo-cli
2) Tick Repositories/Write (required)
3) Tick Repositories/Admin (required to create and update)
4) Tick Repositories/Delete (optional, required to delete)
5) Click Create
6) Copy the resulting token and paste it into your configuration file

