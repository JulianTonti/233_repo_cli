/*

# Day 172: 2018-Jun-22 (Fri): 

Execute various git commands via shelljs

*/
const path = require('path');
const assert = require('assert');
const sh = require('shelljs');
sh.config.silent = true;

// go to the specified directory
function cd(dir='./')
{
	dir = path.resolve(dir);
	assert(sh.test('-d', dir), `ERROR: specified path is not a directory: ${dir}`);
	sh.cd(dir);
}

// initialise a directory (nothing will happen if already under version control)
function init(dir='.')
{
	cd(dir);
	sh.exec('git init');
}

// get a list of remotes for a repo
function get_remotes(dir='./')
{
	cd(dir);

	// get existing remotes
	let remotes = {};
	
	sh.exec('git remote -v').stdout.trim().split("\n").forEach(line => {
		let [name,url,action] = line.split(/[\t ]/);
		remotes[name] = url;
	});
	return remotes;
}

// push from a local directory to a remote repo. Modifies remote if necessary
function push({local='./', remote_name='origin', remote_url='', message='initial commit'})
{
	cd(local);

	// add and commit changes
	sh.exec(`git init`);
	sh.exec(`git add .`);
	sh.exec(`git commit -m "${message}"`);

	// get the URL for the specified remote_name
	let url = get_remotes()[remote_name];

	// new remote, new address
	if (url === undefined)
	{
		sh.exec(`git remote add ${remote_name} ${remote_url}`);
		sh.exec(`git push -u ${remote_name} master`);
	}
	// existing remote, new address
	else if (url != remote_url)
	{
		sh.exec(`git remote rename ${remote_name} old_${remote_name}`);
		sh.exec(`git remote add ${remote_name} ${remote_url}`);
		sh.exec(`git push -u ${remote_name} --all`);
		sh.exec(`git push -u ${remote_name} --tags`);
	}
	// existing remote, existing address
	else
	{
		sh.exec(`git push -u ${remote_name} master`);
	}
}

// pull from the remote repo
function pull(dir='.', remote='origin')
{
	cd(dir);
	sh.exec(`git pull ${remote}`);
}

// clone from a remote directory to a local directory
function clone(remote, local='')
{
	sh.exec(`git clone ${remote} ${local}`);
}

module.exports = { init, push, pull, clone };
