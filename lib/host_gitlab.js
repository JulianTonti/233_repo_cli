/*

# Day 170: 2018-Jun-20 (Wed): 

Functions for interacting with GitLab repositories.

*/
const sh = require('shelljs');// sh.config.silent = true;
const request = require('request');
const readline = require('readline');
const sync = require('./sync');
const NOOP = ()=>{}

// see: https://docs.gitlab.com/ee/api/projects.html#edit-project
const default_params = {
	//"id"                                               : 0, //required and can't be changed later
	"name"                                             : "",
	"path"                                             : "",
	"description"                                      : "",
	"visibility"                                       : "private",
	"tag_list"                                         : [],
	"default_branch"                                   : "master",

	"issues_enabled"                                   : true,
	"snippets_enabled"                                 : true,
	"wiki_enabled"                                     : true,
	"jobs_enabled"                                     : true,
	"shared_runners_enabled"                           : true,
	"request_access_enabled"                           : false,
	"container_registry_enabled"                       : true,
	"resolve_outdated_diff_discussions"                : false,
	"lfs_enabled"                                      : true,

	"merge_method"                                     : "merge",
	"merge_requests_enabled"                           : true,
	"printing_merge_request_link_enabled"              : true,
	"only_allow_merge_if_pipeline_succeeds"            : false,
	"only_allow_merge_if_all_discussions_are_resolved" : false,

	"ci_config_path"                                   : null,

	// public_builds,
	// approvals_before_merge,
	// repository_storage,
	// external_authorization_classification_label
};

// return a copy of the default parameters for this host
function get_default_params()
{
	return Object.assign({}, default_params);
}

// launch a CLI to prompt a user for gitlab username and access token
function authenticate(callback=NOOP)
{
	// instructions for creating a private key
	console.log(`
Permission:

  A personal access token is required for this CLI helper to have access to 
  your account. IT IS YOUR RESPONSIBILITY to manage your access keys and to 
  control how much access the key provides. If you think that a key may have 
  been compromised, revoke it immediately at:

  https://gitlab.com/profile/personal_access_tokens


Official Documentation:

  https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

Manual Setup:

  Create token
  - open a browser and sign in to your account at https://gitlab.com/
  - browse to https://gitlab.com/profile/personal_access_tokens
  - enter a name for the token, eg: repo-cli
  - choose an expiry date (leave blank to never expire)
  - tick "Scopes/api" (required)
  - click "Create personal access token"
  - copy the resulting key (you will not be able to see it again)

  Edit config file
  - find the config file .repo-cli.json (it should be in your home directory)
  - edit with a text editor and find the "gitlab" section
  - enter your username and the copied key from above
  - save and close

CLI Setup:

  - enter your username at the command line prompt
  - open the link (if not automatically opened) and sign in
  - create an access key (as for manual setup)
  - enter the key at the command line prompt

`);
	sync(function* (unyield)
	{
		const rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout
		});
		
		let [user] = yield rl.question('Enter GitLab username: ', unyield);
		if (!user) {
			console.log('Setup cancelled.');
			rl.close();
			return callback('','');
		}

		console.log(`open https://gitlab.com/profile/personal_access_tokens`);
		sh.exec(`open https://gitlab.com/profile/personal_access_tokens`);
		
		let [token] = yield rl.question('Enter GitLab token: ', unyield);
		if (!token) {
			console.log('Setup cancelled.');
			rl.close();
			return callback('','');
		}
		rl.close();
		callback(user,token);
	});
}

// get the URL associated with a repo
function get_url({user='', repo=''})
{
	return `https://gitlab.com/${user}/${repo}`;
}

// get a list of all the user's repos, see: https://docs.gitlab.com/ee/api/projects.html#list-user-projects
function list({user='', token='', callback=NOOP})
{
	request(
		{
			url    : `https://gitlab.com/api/v4/users/${user}/projects?per_page=100`,
			method : 'GET',
			json   : true,
			headers : {
				'Content-Type'  : 'application/json',
				'User-Agent'    : 'request',
				'Private-Token' : token
			}
		},
		(error, meta, data) => {
			if (error || meta.statusCode != 200) {
				return callback({error, meta, data}, null);
			}
			data = data.map(item => ({
					id     : item.id,
					name   : item.path_with_namespace,
					info   : item.description,
					access : item.visibility,
					url    : item.web_url,
			}));
			callback(null, data);
		}
	);
}

// get the configuration options for a specified repo, see: https://docs.gitlab.com/ee/api/projects.html#get-single-project
function select({user='', repo='', token='', callback=NOOP})
{
	request(
		{
			url    : `https://gitlab.com/api/v4/projects/${encodeURIComponent(user + '/' + repo)}`,
			method : 'GET',
			json   : true,
			headers : {
				'User-Agent'    : 'request',
				'Private-Token' : token
			}
		},		
		(error, meta, data) => {
			if (error || meta.statusCode != 200) {
				callback({error, meta, data}, null);
			} else {
				callback(null, data);
			}
		}
	);
}

// change a remote repo parameter, see: https://docs.gitlab.com/ee/api/projects.html#edit-project
function modify({user='', repo='', token='', conf={}, callback=NOOP})
{
	conf.id = encodeURIComponent(user + '/' + repo);

	request(
		{
			url    : `https://gitlab.com/api/v4/projects/${encodeURIComponent(user + '/' + repo)}`,
			method : 'PUT',
			json   : true,
			headers : {
				'User-Agent'    : 'request',
				'Content-Type'  : 'application/json',
				'Private-Token' : token
			},
			body : conf
		},
		(error, meta, data) => {
			if (error || meta.statusCode != 200) {
				callback({error, meta, data}, null);
			} else {
				callback(null,data);
			}
		}
	);
}

// set access for a repo
function set_access({user='', repo='', token='', access='private', callback=NOOP})
{
	switch (access) {
		case 'public':
			modify({user, repo, token, conf:{visibility:'public'}, callback});
		break;
		case 'private':
			modify({user, repo, token, conf:{visibility:'private'}, callback});
		break;
		case 'internal':
			modify({user, repo, token, conf:{visibility:'internal'}, callback});
		break;
		default:
			callback({error:'ERROR: only public|private|internal are supported'}, null);
		break;
	};
}

// create a new remote repo, see: https://docs.gitlab.com/ee/api/projects.html#create-project
function create({user='', repo='', token='', conf={}, callback=NOOP})
{
	let params = Object.assign({}, default_params, conf);
	params.name = repo;
	//params.description = repoinfo;
	//params.visibility = repoaccess;

	request(
		{
			url    : 'https://gitlab.com/api/v4/projects',
			method : 'POST',
			json   : true,
			headers : {
				'User-Agent'    : 'request',
				'Content-Type'  : 'application/json',
				'Private-Token' : token
			},
			body : params
		},
		(error, meta, data) => {
			(error || meta.statusCode != 201)
			? callback({error, meta, data}, null)
			: callback(null, data);
		}
	);
}

// delete a remote repo, see: https://docs.gitlab.com/ce/api/projects.html#remove-project
function destroy({user='', repo='', token='', callback=NOOP})
{
	request(
		{
			url    : `https://gitlab.com/api/v4/projects/${encodeURIComponent(user + '/' + repo)}`,
			method : 'DELETE',
			json   : true,
			headers : {
				'User-Agent'    : 'request',
				'Private-Token' : token
			}
		},
		(error, meta, data) => {
			(error || meta.statusCode != 202)
			? callback({error, meta, data}, null)
			: callback(null, data);
		}
	);
}
module.exports = { get_default_params, authenticate, get_url, list, select, modify, create, destroy, set_access };
