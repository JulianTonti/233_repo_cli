/*

# Day 170: 2018-Jun-20 (Wed): 

Functions for interacting with GitHub repositories.

*/
/*

# Day 170: 2018-Jun-20 (Wed): 

Functions for interacting with GitHub repositories.

*/
const request = require('request');
const sync = require('./sync.js');
const NOOP = ()=>{}

// see: https://developer.github.com/v3/repos/#create
const default_params = {
	"name"               : "",
	"description"        : "",
	"homepage"           : "",
	"private"            : true,
	"has_issues"         : true,
	"has_projects"       : true,
	"has_wiki"           : true,
	"default_branch"     : "master",
	"allow_squash_merge" : true,
	"allow_merge_commit" : true,
	"allow_rebase_merge" : true,
	"archived"           : false,
};

// return a copy of the default parameters for this host
function get_default_params() {
	return Object.assign({}, default_params);
}

// get the URL associated with a repo
function get_url({user='', repo='', token=''})
{
	return (token
		? `https://${token}:x-oauth-basic@github.com/${user}/${repo}`
		: `https://github.com/${user}/${repo}`
	);
}

// launch a CLI to prompt a user for bitbucket username and access token
function authenticate(callback=NOOP)
{
	// instructions for creating a private key
	console.log(`
Permission:

  A personal access token is required for this CLI helper to have access to 
  your account. IT IS YOUR RESPONSIBILITY to manage your access keys and to 
  control how much access the key provides. If you think that a key may have 
  been compromised, revoke it immediately at:

  https://github.com/settings/tokens/


Official Documentation:

  https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line/

Manual Setup:

  Create token
  - open a browser and sign in to your account at https://github.com/
  - browse to https://github.com/settings/tokens/new
  - enter a description for the token (eg: repo-cli)
  - tick "repo" (required)
  - tick "delete_repo" (optional)
  - click "Generate Token"
  - copy the resulting key (you will not be able to see it again)

  Edit config file
  - find the config file .repo-cli.json (it should be in your home directory)
  - edit with a text editor and find the "github" section
  - enter your username and the copied key from above
  - save and close

CLI Setup:

  - enter your username at the command line prompt
  - open the link (if not automatically opened) and sign in
  - create an access key (as for manual setup)
  - enter the key at the command line prompt

`);
	const sh = require('shelljs');
	sh.config.silent = true;
	const readline = require('readline');

	sync(function* (unyield)
	{
		const rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout
		});
		
		let [user] = yield rl.question('Enter GitHub username: ', unyield);
		if (!user) {
			console.log('Setup cancelled.');
			rl.close();
			return callback('','');
		}

		console.log(`open https://github.com/settings/tokens/new`);
		sh.exec(`open https://github.com/settings/tokens/new`);
		
		let [token] = yield rl.question('Enter GitHub token: ', unyield);
		if (!token) {
			console.log('Setup cancelled.');
			rl.close();
			return callback('','');
		}
		rl.close();
		callback(user,token);
	});
}

// get a list of all the user's repos, see: https://developer.github.com/v3/repos/#list-your-repositories
function list({user='', token='', callback=NOOP})
{
	request(
		{
			url    : 'https://api.github.com/user/repos',
			method : 'GET',
			json   : true,
			headers : {
				'User-Agent'    : 'request',
				'Authorization' : `token ${token}`
			}
		},
		(error, meta, data) => {
			if (error || meta.statusCode != 200) {
				return callback({error, meta, data}, null);
			}
			data = data.map(item => ({
				id     : item.id,
				name   : item.full_name,
				info   : item.description,
				access : item.private ? 'private' : 'public',
				url    : item.html_url,
			}));
			callback(null, data);
		}
	);
}

// get the configuration options for a specified repo, see: https://developer.github.com/v3/repos/#get
function select({user='', repo='', token='', callback=NOOP})
{
	request(
		{
			url     : `https://api.github.com/repos/${user}/${repo}`,
			method  : 'GET',
			json    : true,
			headers : {
				'User-Agent'    : 'request',
				'Authorization' : `token ${token}`
			}
		},
		(error, meta, data) => {
			if (error || meta.statusCode != 200) {
				callback({error, meta, data}, null);
			} else {
				callback(null,data);
			}
		}
	);
}

// change a remote repo parameter, see: https://developer.github.com/v3/repos/#edit
function modify({user='', repo='', token='', conf={}, callback=NOOP})
{
	request(
		{
			url     : `https://api.github.com/repos/${user}/${repo}`,
			method  : 'PATCH',
			json    : true,
			headers : {
				'User-Agent'    : 'request',
				'Authorization' : `token ${token}`,
				'Content-Type'  : 'application/json',
			},
			body : conf
		},
		(error, meta, data) => {
			if (error || meta.statusCode != 200) {
				callback({error, meta, data}, null);
			} else {
				callback(null, data);
			}
		}
	);
}

// set access for a repo
function set_access({user='', repo='', token='', access='private', callback=NOOP})
{
	switch (access) {
		case 'public':
			modify({user, repo, token, conf:{private:false}, callback});
		break;
		case 'private':
			modify({user, repo, token, conf:{private:true}, callback});
		break;
		default:
			callback({error:'ERROR: only public|private are supported'}, null);
		break;
	};
}

// create a new remote repo, see: https://developer.github.com/v3/repos/#create
function create({user='', repo='', token='', conf={}, callback=NOOP})
{
	let params = Object.assign({}, default_params, conf);
	params.name = repo;

	request(
		{
			url     : 'https://api.github.com/user/repos',
			method  : 'POST',
			json    : true,
			headers : {
				'User-Agent'    : 'request',
				'Authorization' : `token ${token}`,
				'Content-Type'  : 'application/json',
			},
			body : params
		},
		(error, meta, data) => {
			(error || meta.statusCode != 201)
			? callback({error, meta, data}, null)
			: callback(null, data);
		}
	);
}

// delete a remote repo, see: https://developer.github.com/v3/repos/#delete-a-repository
function destroy({user='', repo='', token='', callback=NOOP})
{
	request(
		{
			url     : `https://api.github.com/repos/${user}/${repo}`,
			method  : 'DELETE',
			json   : true,
			headers : {
				'User-Agent'    : 'request',
				'Authorization' : `token ${token}`
			}
		},
		(error, meta, data) => {
			(error || meta.statusCode != 204)
			? callback({error, meta, data}, null)
			: callback(null, data);
		}
	);
}
module.exports = { get_default_params, get_url, authenticate, list, select, modify, create, destroy, set_access };

