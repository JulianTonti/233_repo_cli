/*

# Day 172: 2018-Jun-22 (Fri):

Helper for working with async code.

Yield when calling a function that has a callback and set the callback for that
function to be the unyield function provided here. Callback params will be 
passed back as an array that can be destructured.

Examples:

sync(function* (unyield)
{
	// takes 3 seconds to execute
	yield setTimeout(unyield, 1000);
	yield setTimeout(unyield, 1000);
	yield setTimeout(unyield, 1000);

	// new request only happens when previous request finishes
	let [error1,response1] = yield http_get(options, unyield);
	let [error2,response2] = yield http_get(options, unyield);
	let [error3,response3] = yield http_get(options, unyield);

	// files are loaded one by one, even though the calls are async
	let [data1] = readFileAsync(filepath1, unyield);
	let [data2] = readFileAsync(filepath2, unyield);
	let [data3] = readFileAsync(filepath3, unyield);

	// processes run in parallel, but the output waits for all to finish
	for (let i=0; i<10; ++i) launch_worker(i, unyield);
	for (let i=0; i<10; ++i) let [result] = yield;
});

*/
const assert = require('assert');

function sync(gen_src)
{
	assert(
		gen_src && gen_src.constructor.name == 'GeneratorFunction',
		'ERROR: function sync() expects a generator function as input'
	);

	// the unyield function intercepts a callback and passes input params back out through the generator
	function unyield(...args)
	{
		// allow the event loop to clear before passing (avoids blowing the stack)
		setImmediate(() => gen.next(args));
	}

	// create the generator, initialising it with the unyield function
	let gen = gen_src(unyield);

	// start the generator
	gen.next();
}
module.exports = sync;


