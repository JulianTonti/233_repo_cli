// wrap text at a fixed number of characters
function to_lines(num,text)
{
	let lines = [];
	let line = [];

	let words = text.split(' ');

	let linelength = 0;

	for (let word of words)
	{
		if (linelength + word.length <= num) {
			line.push(word);
			linelength += word.length;
		}
		else {
			lines.push(line.join(' '));
			line = [word];
			linelength = word.length;
		}
	}
	lines.push(line.join(' '));
	return lines;
}
module.exports = { to_lines };