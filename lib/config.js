/*

# Day 172: 2018-Jun-22 (Fri):

Base configuration for the repo-cli. This will be used to construct a user
specific JSON configuration file on the first run.

By default, a user's config file will be created in their home directory as:

{user_home_directory}/.repo-cli.json

All listed params can be overridden, either by the user's config.json, or by
key=val pairs provided with the command line call. The list of params is not
exhaustive and any extra params provided by the user will be passed through
without modification.

Certain params are common between all providers. These have been mapped to a
common name and are all REQUIRED. When invoking a call to a specific provider,
custom logic in the associated library will map the common name back to the
correct name for that provider.

All other params are specific to the chosen provider and are OPTIONAL. If a
param's value is set to null in this file, then the key will not be copied
across when creating a user's config.json file. Hence, the list of params here
is considered only advisory.

Consult the listed URLs for more information about each RESTful API provided
by the GitHub, GitLab and BitBucket

GitHub:
	https://developer.github.com/v3/repos/#create

GitLab:
	https://docs.gitlab.com/ee/api/projects.html#create-project

BitBucket:
	https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D#post,

*/
const os = require('os');
const fs = require('fs');
const sh = require('shelljs');
const path = require('path');
const file = path.join(os.homedir(), ".repo-cli.json");
const hosts = {
	github    : require('./host_github.js'),
	gitlab    : require('./host_gitlab.js'),
	bitbucket : require('./host_bitbucket.js')
};
const conf = {}; // host : {username, token, options }

// create a new (default) config file
function create()
{
	for (let host of Object.keys(hosts)) {
		conf[host] = {
			host   : host,
			user   : '',
			token  : '',
			params : hosts[host].get_default_params()
		};
	}
	save();
}

// save config to file
function save(data=null)
{
	fs.writeFileSync(file, JSON.stringify(data || conf,null,2));
}

// load config from file
function load()
{
	if (sh.test('-f', file) === false) {
		create();
	}
	else {
		let data = JSON.parse(fs.readFileSync(file));

		for (let host of Object.keys(hosts)) {
			if (data[host]) {
				conf[host] = data[host];
			}
		}
	}
	return conf;
}

// print the user config to the terminal in an easily readable form
function print()
{
	console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");

	for (let group of Object.keys(conf))
	{
		for (let field of Object.keys(config[group]))
		{
			console.log(`${group}.${field} = ${config[group][field]}`);
		}
		console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
	}
}

module.exports = { load, save, create, print, conf };
