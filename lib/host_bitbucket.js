/*

# Day 170: 2018-Jun-20 (Wed): 

Functions for interacting with BitBucket repositories.

*/
const sh = require('shelljs');// sh.config.silent = true;
const request = require('request');
const readline = require('readline');
const sync = require('./sync');
const NOOP = ()=>{}

// see: https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D#post
const params_default = {
	"is_private"  : true,
	"scm"         : "git", //hg, git
	"name"        : "",
	"description" : "",
	"language"    : "",
	"has_issues"  : false,
	"has_wiki"    : false,
	"fork_policy" : "no_public_forks", //allow_forks, no_public_forks, no_forks
	"website"     : "",
};

// return a copy of the default parameters for this host
function get_default_params()
{
	return Object.assign({}, params_default);
}

// launch a CLI to prompt a user for bitbucket username and access token
function authenticate(callback=NOOP)
{
	// instructions for creating a private key
	console.log(`
Permission:

  A personal access token is required for this CLI helper to have access to 
  your account. IT IS YOUR RESPONSIBILITY to manage your access keys and to 
  control how much access the key provides. If you think that a key may have 
  been compromised, revoke it immediately at:

  https://bitbucket.org/account/user/YOUR_USERNAME/app-passwords


Official Documentation:

  https://confluence.atlassian.com/bitbucketserver/personal-access-tokens-939515499.html


Manual Setup:

  Create token
  - open a browser and sign in to your account at https://bitbucket.org/
  - browse to https://bitbucket.org/account/user/YOUR_USERNAME/app-passwords/new
  - enter a name for the token (eg: repo-cli)
  - tick Repositories/Write (required)
  - tick Repositories/Admin (required to create and update)
  - tick Repositories/Delete (optional, required to delete)
  - click Create
  - copy the resulting key (you will not be able to see it again)

  Edit config file
  - find the config file .repo-cli.json (it should be in your home directory)
  - edit with a text editor and find the "bitbucket" section
  - enter your username and the copied key from above
  - save and close

CLI Setup:

  - enter your username at the command line prompt
  - open the link (if not automatically opened) and sign in
  - create an access key (as for manual setup)
  - enter the key at the command line prompt

`);
	sync(function* (unyield)
	{
		const rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout
		});
		
		let [user] = yield rl.question('Enter BitBucket username: ', unyield);
		if (!user) {
			console.log('Setup cancelled.');
			rl.close();
			return callback('','');
		}

		console.log(`browse to: https://bitbucket.org/account/user/${user}/app-passwords/new`);
		sh.exec(`open https://bitbucket.org/account/user/${user}/app-passwords/new`);
		
		let [token] = yield rl.question('Enter BitBucket token: ', unyield);
		if (!token) {
			console.log('Setup cancelled.');
			rl.close();
			return callback('','');
		}
		rl.close();
		callback(user,token);
	});
}

// get the URL associated with a repo
function get_url({user='', repo=''})
{
	return `https://bitbucket.org/${user}/${repo}`;
}

// get a list of all the user's repos, see: https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories
function list({user='', token='', callback=NOOP})
{
	request(
		{
			url    : `https://api.bitbucket.org/2.0/repositories/${user}?pagelen=100`,
			method : 'GET',
			json   : true,
			headers : {
				'User-Agent'    : 'request',
				'Authorization' : `Bearer ${token}`
			},
			auth : {
				'username' : user,
				'password' : token
			}
		},
		(error, meta, data) => {
			if (error || meta.statusCode != 200) {
				return callback({error, meta, data}, null);
			}
			data = data.values.map(item => ({
				scm    : item.scm,
				name   : item.full_name,
				info   : item.description,
				access : item.is_private ? 'private' : 'public',
				url    : item.links.html.href
			}));
			callback(null, data);
		}
	);
}

// get the configuration options for a specified repo, see: https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D
function select({user='', repo='', token='', callback=NOOP})
{
	request(
		{
			url    : `https://api.bitbucket.org/2.0/repositories/${user}/${repo}`,
			method : 'GET',
			json   : true,
			headers : {
				'User-Agent' : 'request',
			},
			auth : {
				'username' : user,
				'password' : token
			}
		},
		(error, meta, data) => {
			if (error || meta.statusCode != 200) {
				callback({error, meta, data}, null);
			} else {
				callback(null, data);
			}
		}
	);
}

// change a remote repo parameter, see: https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D#put
function modify({user='', repo='', token='', conf={}, callback=NOOP})
{
	request(
		{
			url    : `https://api.bitbucket.org/2.0/repositories/${user}/${repo}`,
			method : 'PUT',
			json   : true,
			headers : {
				'User-Agent' : 'request',
				'Content-Type'  : 'application/json',
				'Authorization' : `Bearer ${token}`
			},
			auth : {
				'username' : user,
				'password' : token
			},
			body : conf
		},
		(error, meta, data) => {
			if (error || meta.statusCode != 200) {
				callback({error, meta, data}, null);
			} else {
				callback(null, data);
			}
		}
	);
}

// set access for a repo
function set_access({user='', repo='', token='', access='private', callback=NOOP})
{
	switch (access) {
		case 'public':
			modify({user, repo, token, conf:{is_private:false}, callback});
		break;
		case 'private':
			modify({user, repo, token, conf:{is_private:true}, callback});
		break;
		default:
			callback({error:'ERROR: only public|private are supported'}, null);
		break;
	};
}

// create a new remote repo, see: https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D#post
function create({user='', repo='', token='', conf={}, callback=NOOP})
{
	let params = Object.assign({}, params_default, conf);
	params.name = repo;
	//params.scm = 'git';
	//params.is_private  = repoaccess == 'private';
	//params.description = repoinfo;
	request(
		{
			url    : `https://api.bitbucket.org/2.0/repositories/${user}/${repo.toLowerCase()}`,
			method : 'POST',
			json   : true,
			headers : {
				'User-Agent'    : 'request',
				'Content-Type'  : 'application/json'
				//'Authorization' : `Bearer ${token}`
			},
			auth : {
				'username' : user,
				'password' : token
			},
			body : params
		},
		(error, meta, data) => {
			(error || meta.statusCode != 200)
			? callback({error, meta, data}, null)
			: callback(null, data);
		}
	);
}

// delete a remote repo, see: https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D#delete
function destroy({user='', repo='', token='', callback=NOOP})
{
	request(
		{
			url    : `https://api.bitbucket.org/2.0/repositories/${user}/${repo}`,
			method : 'DELETE',
			json   : true,
			headers : {
				'User-Agent'    : 'request',
			},
			auth : {
				'username' : user,
				'password' : token
			}
		},
		(error, meta, data) => {
			(error || meta.statusCode != 204)
			? callback({error, meta, data}, null)
			: callback(null, data);
		}
	);
}
module.exports = { get_default_params, authenticate, get_url, list, select, modify, set_access, create, destroy };
