/*

# Day 187: 2018-Jul-07 (Sat): 

Interact with remote repos at github, gitlab or bitbucket

*/
const fs = require('fs');
const sh = require('shelljs');
const git = require('./git.js');
const sync = require('./sync.js');
const path = require('path');
const HOSTS = {
	github    : require('./host_github.js'),
	gitlab    : require('./host_gitlab.js'),
	bitbucket : require('./host_bitbucket.js')
};
const config = require('./config.js');
config.load();
const CONFIG = {
	github : config.conf.github,
	gitlab : config.conf.gitlab,
	bitbucket : config.conf.bitbucket
};

// in response to a failed HTTPS request, write error information and exit
function die(message,state)
{
	console.error(message);
	if (state) {
		console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
		if (state.meta) console.error(`HTTP ${state.meta.statusCode}: ${state.meta.statusMessage}`);
		if (state.error) console.error(JSON.stringify(state.error,null,2));
		if (state.data) console.error(JSON.stringify(state.data,null,2));
		console.error('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
	}
	process.exit();
}

// load the library for a specified hostname
function load_host(remote)
{
	let [hostname, reponame] = remote.split("://");

	if (HOSTS[hostname] === undefined) {
		die(`ERROR: unsupported host. Legal values are ${Object.keys(HOSTS).join('|')}`);
	}
	return [HOSTS[hostname], CONFIG[hostname], hostname, reponame];
}

// get the URL for a remote repo
function get_url(remote)
{
	let [host,conf,hostname,reponame] = load_host(remote);
	return host.get_url({
		user  : conf.user,
		repo  : reponame,
		token : conf.token
	});
}

// launch a command-line interface for assisting token creation, then save to settings
function authenticate(remote)
{
	let [host,conf] = load_host(remote);
	host.authenticate((username,token) => {
		if (!username || !token) return;
		conf.user = username;
		conf.token = token;
		config.save();
	});
}

// get a list of remote repos
function list(remote, callback)
{
	sync(function*(unyield) {
		let [host,params,hostname,reponame] = load_host(remote);
		let [error,data] = yield host.list({
			user     : params.user,
			token    : params.token,
			callback : unyield
		});
		if (error)
		{
			return callback({
				message : `FAILURE: unable to list repos from ${remote}.`, 
				state : error
			}, null);
		}
		else
		{
			return callback(null, data.map(item => ({
				access : item.access,
				name   : `${hostname}://${item.name.split('/').pop()}`,
				info   : item.info || 'no description provided',
				url    : item.url
			})));
		}
	});
}

// go to the webpage for a specific remote repo
function open(remote) {
	sh.exec(`open ${get_url(remote)}`);
}

// determine whether or not a remote repo already exists
function exists(remote, callback)
{
	list(remote, (error,data) => {
		if (error) return callback(error,null);
		data = data.filter(item => item.name == remote);
		callback(null, data.length == 1);
	});
}

// create an empty remote repo
function create(remote, userconf={}, callback)
{
	sync(function*(unyield) {
		let [host,params,hostname,reponame] = load_host(remote);
		let repoconf = Object.assign({}, params.options, userconf);
		let [error,data] = yield host.create({
			user     : params.user,
			repo     : reponame,
			token    : params.token,
			conf     : repoconf,
			callback : unyield
		});
		callback(error ? {message:`FAILURE: unable to create repo ${remote}.`, state:error} : null, null);
	});
}

// destroy (delete) a remote repo
function destroy(remote, callback)
{
	sync(function*(unyield) {
		let [host,params,hostname,reponame] = load_host(remote,true);
		let [error,data] = yield host.destroy({
			user     : params.user,
			repo     : reponame,
			token    : params.token,
			callback : unyield
		});
		callback(error ? {message:`FAILURE: unable to delete repo ${remote}.`, state:error} : null, null);
	});
}

// get the current configuration for a selected repo
function select(remote, callback)
{
	let [host,conf,hostname,reponame] = load_host(remote);

	// if no reponame is provided, print the default config for the host
	if (!reponame) {
		console.log('DEFAULT OPTIONS:');
		console.log(JSON.stringify(conf,null,2));
		return callback(null,null);
	}

	// otherwise, get the config for the specified repo
	sync(function*(unyield) {
		let [error,data] = yield host.select({
			user     : conf.user,
			repo     : reponame,
			token    : conf.token,
			callback : unyield
		});
		if (error) {
			callback({message:`FAILURE: unable to select repo configuration for ${remote}.`, state:error});
		}
		else {
			// configurable options (a subset of 'data')
			let config = Object.assign({}, conf.params);

			Object.keys(config).forEach(key => {
				config[key] = data[key];
				if (config[key] === undefined) delete config[key];
			});
			callback(null, {data,config});
		}
	});
}

// modify settings for a repo (if no repo is provided, then the defaults will be modified)
function modify(remote, options={}, callback)
{
	let [host,conf,hostname,reponame] = load_host(remote,true);

	// if no reponame is provided, modify the default settings
	if (!reponame)
	{
		Object.keys(options).forEach(key => {
			if (conf.params[key] !== undefined) conf.params[key] = options[key];
		});
		config.save();
		return callback(null,null);
	}

	sync(function*(unyield) {
		let [error,data] = yield host.modify({
			user     : conf.user,
			repo     : reponame,
			token    : conf.token,
			conf     : options,
			callback : unyield
		});
		callback(error ? {message:`FAILURE: unable to modify repo ${remote}.`, state:error} : null, null);
	});
}

// set visibility for a repo (usually public or private)
function set_access(remote, access, callback)
{
	let [host,conf,hostname,reponame] = load_host(remote,true);

	sync(function*(unyield) {
		let [error] = yield host.set_access({
			user     : conf.user,
			repo     : reponame,
			token    : conf.token,
			access   : access,
			callback : unyield
		});
		callback(error ? {message:`FAILURE: unable to modify repo ${remote}.`, state:error} : null, null);
	});
}

// verify that a remote repo and local directory are compatible
function verify(remote, local, must_be_empty=false)
{
	local = path.resolve(local);
	let reponame = load_host(remote)[3];
	let localname = path.basename(local);

	// local directory name must match remote repo name
	if (localname !== reponame) {
		console.error(`ERROR: local directory name (${localname}) must match repo name: ${reponame}`);
		process.exit(1);
	}

	// local directory must exist
	if (sh.test('-e', local) === false) {
		console.error(`ERROR: specified local path does not exist: ${local}`);
		process.exit(1);
	}

	// local path must be a directory
	if (sh.test('-d', local) === false) {
		console.error(`ERROR: local path does not point to a directory: ${local}`);
		process.exit(1);
	}

	// if required, local directory must be empty
	if (must_be_empty && sh.ls(local) + '' != '') {
		console.error(`ERROR: local directory is not empty: ${local}`);
		process.exit(1);
	}
}

// clone from the designated remote repo to designated local directory
function clone(remote, local='./')
{
	verify(remote,local,true);
	git.clone(
		get_url(remote),
		path.resolve(local)
	);
}

// push changes for the specified directory
function push(remote, local='./', message='automatic update')
{
	verify(remote,local);
	git.push({
		local       : path.resolve(local),
		remote_name : 'origin',
		remote_url  : get_url(remote),
		message     : message
	});
}

// pull changes for the specified directory
function pull(remote, local='./')
{
	verify(remote,local);
	git.pull(path.resolve(local),'origin');
}

module.exports = { 
	authenticate, // set username and access token for a repo host
	list,   // list the repos at a host
	get_url,// get the URL of a host
	exists, // true if the repo already exists
	open,   // open a repo in the default web browser
	select, // print a list of remote repo settings
	modify, // modify remote repo settings
	create, // create a new remote repo from a local directory
	destroy, // delete a remote repo
	set_access, // set visibility of a repo
	clone,  // wrapper for git clone
	pull,   // wrapper for git pull
	push,   // wrapper for git commit * then git push
};
